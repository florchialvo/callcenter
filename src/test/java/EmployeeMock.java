import domain.Employee;

public class EmployeeMock extends Employee {

    @Override
    public int attentionTime() {
        return 5;
    }
}

import domain.CallCenter;
import domain.Department;
import domain.Dispatcher;
import domain.Employee;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CallCenterTest {

    private Dispatcher dispatcher;

    private CallCenter callCenter;

    @Before
    public void init() throws Exception {

        Department operatorsDepartment = new Department();
        List<Employee> operators = new ArrayList<>();
        operators.add(new EmployeeMock());
        operators.add(new EmployeeMock());
        operators.add(new EmployeeMock());
        operators.add(new EmployeeMock());
        operatorsDepartment.setEmployees(operators);

        Department supervisorDepartment = new Department();
        List<Employee> supervisors = new ArrayList<>();
        supervisors.add(new EmployeeMock());
        supervisors.add(new EmployeeMock());
        supervisors.add(new EmployeeMock());
        supervisors.add(new EmployeeMock());
        supervisorDepartment.setEmployees(supervisors);

        Department directorsDepartment = new Department();
        List<Employee> directors = new ArrayList<>();
        directors.add(new EmployeeMock());
        directors.add(new EmployeeMock());
        directorsDepartment.setEmployees(directors);

        callCenter = new CallCenter(operatorsDepartment, supervisorDepartment, directorsDepartment);

        dispatcher = new Dispatcher(callCenter, 10);
    }


    @Test
    public void dispatchTenCallsWithTenEmployees() throws Exception {


        for (int i = 0; i < 10; i++) {
            dispatcher.dispatchCall();
        }

        dispatcher.close();

        Assert.assertEquals(10, dispatcher.getAttendedCalls());
        Assert.assertEquals(0, dispatcher.getNotAttendedCalls());

    }

    @Test
    public void dispatchMoreCallsThanThreadsAndAttendAll() throws Exception {


        for (int i = 0; i < 11; i++) {
            dispatcher.dispatchCall();
        }

        dispatcher.close();

        Assert.assertEquals(11, dispatcher.getAttendedCalls());
        Assert.assertEquals(0, dispatcher.getNotAttendedCalls());

    }

    @Test
    public void dispatchCallsPerSecondWithTenEmployees() throws Exception {


        for (int i = 0; i < 10; i++) {
            Thread.sleep(1000);
            dispatcher.dispatchCall();
        }

        dispatcher.close();

        Assert.assertEquals(10, dispatcher.getAttendedCalls());
        Assert.assertEquals(0, dispatcher.getNotAttendedCalls());
        Assert.assertEquals(8, callCenter.getOperators().getAttendedCalls().get());
        Assert.assertEquals(2, callCenter.getSupervisors().getAttendedCalls().get());
        Assert.assertEquals(0, callCenter.getDirectors().getAttendedCalls().get());

    }

    @Test
    public void dispatchFiveCallsWithFourEmployees() throws Exception {

        List<Employee> supervisors = new ArrayList<>();
        supervisors.add(new EmployeeMock());
        supervisors.add(new EmployeeMock());

        List<Employee> operators = new ArrayList<>();
        operators.add(new EmployeeMock());
        operators.add(new EmployeeMock());

        Department operatorsDepartment = new Department();
        operatorsDepartment.setEmployees(operators);

        Department supervisorDepartment = new Department();
        supervisorDepartment.setEmployees(supervisors);

        callCenter.setDirectors(null);
        callCenter.setSupervisors(supervisorDepartment);
        callCenter.setOperators(operatorsDepartment);

        for (int i = 0; i < 5; i++) {
            Thread.sleep(1000);
            dispatcher.dispatchCall();
        }

        dispatcher.close();

        Assert.assertEquals(4, dispatcher.getAttendedCalls());
        Assert.assertEquals(1, dispatcher.getNotAttendedCalls());

    }
}


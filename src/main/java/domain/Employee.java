package domain;

import java.util.Random;

public class Employee {

    private boolean available = true;

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }


    public void attend() throws InterruptedException {
        setAvailable(false);
        Thread.sleep(attentionTime()*1000);
        setAvailable(true);

    }

    public int attentionTime() {
        Random random = new Random();
        return (random.nextInt(6) + 5);
    }
}

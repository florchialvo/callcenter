package domain;

import exceptions.CantAttendCallException;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class CallCenter {

    public CallCenter(Department operators, Department supervisors, Department directors) {
        setDirectors(directors);
        setSupervisors(supervisors);
        setOperators(operators);

    }

    private Department operators;

    private Department supervisors;

    private Department directors;

    public Department getOperators() {
        return operators;
    }

    public void setOperators(Department operators) {
        this.operators = operators;
        operators.setNextDepartment(supervisors);
    }

    public Department getSupervisors() {
        return supervisors;
    }

    public void setSupervisors(Department supervisors) {
        this.supervisors = supervisors;
        supervisors.setNextDepartment(directors);
    }

    public Department getDirectors() {
        return directors;
    }

    public void setDirectors(Department directors) {
        this.directors = directors;
    }

    public void attendPhoneCall() throws CantAttendCallException, InterruptedException {
        operators.attendPhoneCall();
    }
}

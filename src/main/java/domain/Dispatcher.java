package domain;

import exceptions.CantAttendCallException;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class Dispatcher {

    public Dispatcher(CallCenter callCenter, int amountOfCalls) {
        setCallCenter(callCenter);
        this.call = Executors.newFixedThreadPool(amountOfCalls);
    }

    private CallCenter callCenter;

    public final ExecutorService call;

    private final AtomicInteger attendedCalls = new AtomicInteger();

    private final AtomicInteger notAttendedCalls = new AtomicInteger();

    public int getNotAttendedCalls() {
        return notAttendedCalls.get();
    }

    public int getAttendedCalls() {
        return attendedCalls.get();
    }


    public void dispatchCall() {
        call.execute(() -> {
            try {
                callCenter.attendPhoneCall();
                attendedCalls.incrementAndGet();
            } catch (CantAttendCallException e) {
                notAttendedCalls.incrementAndGet();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

    }

    public void close() throws InterruptedException {
        call.shutdown();
        call.awaitTermination(1, TimeUnit.MINUTES);
    }


    public void setCallCenter(CallCenter callCenter) {
        this.callCenter = callCenter;
    }
}

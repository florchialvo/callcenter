package domain;

import exceptions.CantAttendCallException;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Department {

    private Department nextDepartment;

    private List<Employee> employees;

    private final AtomicInteger attendedCalls = new AtomicInteger();

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public Department getNextDepartment() {
        return nextDepartment;
    }

    public void setNextDepartment(Department nextDepartment) {
        this.nextDepartment = nextDepartment;
    }

    public void delegateToEmployee() throws InterruptedException, CantAttendCallException {

        Employee employee;
        synchronized (this) {
            employee = getEmployees().stream().filter(e -> e.isAvailable()).findFirst().orElse(null);
        }


        if (employee != null) {
            attendedCalls.getAndIncrement();
            employee.attend();
        } else {
            throw new CantAttendCallException(Thread.currentThread().getName() + " We are busy now!");
        }

    }

    public void attendPhoneCall() throws InterruptedException, CantAttendCallException {

        try {
            delegateToEmployee();
        } catch (CantAttendCallException e) {
            if (getNextDepartment() == null) {
                throw new CantAttendCallException(e.getMessage());
            }
            getNextDepartment().attendPhoneCall();
        }

    }

    public AtomicInteger getAttendedCalls() {
        return attendedCalls;
    }
}

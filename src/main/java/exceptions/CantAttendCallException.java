package exceptions;

public class CantAttendCallException extends Exception {

    public CantAttendCallException(String s) {
        super(s);
    }
}
